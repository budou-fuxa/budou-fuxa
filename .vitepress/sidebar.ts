import { DefaultTheme } from "vitepress";

/**
 * 指引
 * ------------------------------------------------
 * ------------------------------------------------
 */
const guide: DefaultTheme.SidebarItem = {
	text: "指引",
	collapsed: false,
	items: [
		{ text: "介绍", link: "/guide/introduce" },
		{ text: "特性", link: "/guide/features" },
	],
};
/**
 * 入门
 * ------------------------------------------------
 * ------------------------------------------------
 */
const introduction: DefaultTheme.SidebarItem = {
	text: "入门",
	collapsed: false,
	items: [
		{ text: "开始", link: "/guide/getting-started" },
		{ text: "安装和运行", link: "/guide/installing-and-running" },
		{ text: "如何使用设备和标签", link: "/guide/how-to-devices-and-tags" },
		{ text: "如何使用相同的视图", link: "/guide/how-to-use-same-view" },
		{ text: "如何定义形状", link: "/guide/how-to-define-shapes" },
		{ text: "如何绑定控件", link: "/guide/how-to-bind-controls" },
		{ text: "如何控制图表", link: "/guide/how-to-chart-control" },
		{ text: "如何设计UI布局", link: "/guide/how-to-ui-layout" },
		{ text: "如何设置告警", link: "/guide/how-to-setup-alarms" },
		{ text: "如何动画管道", link: "/guide/how-to-animate-pipe" },
		{ text: "如何保存加载项目", link: "/guide/how-to-save-load-project" },
		{ text: "如何配置事件", link: "/guide/how-to-configure-events" },
		{ text: "如何配置脚本", link: "/guide/how-to-configure-script" },
		{ text: "设置", link: "/guide/settings" },
		{ text: "提示和技巧", link: "/guide/tips-and-tricks" },
	],
};

export const sidebar: DefaultTheme.Sidebar = {
	"/guide": [
		guide,
		introduction
	],
};
