import { DefaultTheme } from "vitepress/types/default-theme";

export const navbar: DefaultTheme.NavItem[] = [
	{
		text: " 介绍",
		link: "/guide/introduce",
		activeMatch: "^/guide/introduce",
	},
	{
		text: "安装",
		link: "/guide/installing-and-running",
		activeMatch: "^/guide/installing-and-running",
	},
	{
		text: "指南",
		link: "/guide/getting-started",
		activeMatch: "^/guide/getting-started",
	},
	{
		text: "相关链接",
		activeMatch: "^/mom/app",
		items: [
			{ text: "svgviewer", link: "https://www.svgviewer.dev/" },
			{ text: "Angular", link: "https://angular.io/" },
			{ text: "Node", link: "https://nodejs.org/" },
		],
	},
];
