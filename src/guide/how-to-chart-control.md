# 如何控制图表

## 图表
要将图表控件添加到视图::进入编辑器并选择视图。
![/images/fuxa-chart.gif](/images/fuxa-chart.gif)
## 线图表
在折线图中，您可以定义要附加到图表控件的图表。
![/images/setup-charts.png](/images/setup-charts.png)
