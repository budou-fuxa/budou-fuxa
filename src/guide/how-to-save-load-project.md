# 如何保存加载项目

## 创建一个项目
你可以创建一个新的项目，当前的项目将被新的void项目覆盖(只有一个void MainView)。
![/images/fuxa-project.png](/images/fuxa-project.png)

## 保存项目
每次更改设置(对话框确认)后，项目将自动保存在内部数据库中，如编辑“设备”，“标签”，“图表”，....通过选择另一个视图或离开编辑器，将自动保存视图。

通过Save Project，您可以强制内部保存过程。通过Save Project As…，你可以将整个项目导出为JSON格式的文件(MyProject.fuxap)，这对于制作项目备份很有用。

## 打开项目
您可以从导出的文件(MyProject.fuxap)中打开一个项目。
