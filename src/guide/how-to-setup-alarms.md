# 如何设置告警

## 告警
在“告警编辑器”中配置告警。
![/images/setup-alarms.png](/images/setup-alarms.png)
一个告警列表绑定一个Tag，可以定义4种不同级别的条件(High、High、Low和Message)。
![/images/fuxa-alarms.gif](/images/fuxa-alarms.gif)
## 布局设置
活动警报和历史记录可以通过配置菜单视图或头部按钮显示。
![/images/fuxa-alarms2.gif](/images/fuxa-alarms2.gif)
也可以为每个警报配置几个动作，比如显示弹出对话框或为标签设置一个值。
![/images/fuxa-alarms3.gif](/images/fuxa-alarms3.gif)
