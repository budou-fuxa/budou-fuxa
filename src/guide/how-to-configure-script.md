# 如何配置脚本

## 脚本
要配置脚本，请转到编辑器中的脚本。
![/images/setup-scripts.png](/images/setup-scripts.png)

添加一个新的脚本(脚本是一个javascript函数)，配置函数名和参数，有2种类型的参数:
* 标签ID
* 值、数字或字符串

然后将逻辑写入函数中。你可以使用以下系统调用:
* `$setTag`: 设置标签的值
* `$getTag`: 用于获取当前标签值
![/images/fuxa-script.gif](/images/fuxa-script.gif)

您可以测试脚本，以验证使用`console.log`方法是否有意义。
![/images/fuxa-script1.gif](/images/fuxa-script1.gif)

## 事件
在GUI中，您可以在Events中配置脚本的调用。
![/images/fuxa-script2.gif](/images/fuxa-script2.gif)
