# 如何绑定控件

## 输出
要将输出控件绑定到设备标签(变量): 进入编辑器并选择视图。
![/images/fuxa-output-control.gif](/images/fuxa-output-control.gif)
## 输入
将输入控件绑定到设备标记。
![/images/fuxa-input-control.gif](/images/fuxa-input-control.gif)
## 选择
将选择控件绑定到设备标记。
![/images/fuxa-select-control.gif](/images/fuxa-select-control.gif)
## 滑块
将滑块控件绑定到设备标签。
![/images/fuxa-slider-control.gif](/images/fuxa-slider-control.gif)
