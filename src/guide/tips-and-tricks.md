# 提示和技巧

键盘快捷键:
* Ctrl +左/右:旋转选中的项目
* Ctrl + Shift +左/右:旋转选中的项目在大步
* Shift + O / P:选择上一项/下一项
* Tab / Shift + Tab:选择上一项/下一项
* Ctrl + Up / Down:中央放大/缩小
* Ctrl + Z / Y:撤销/重做
* Shift +“用鼠标调整所选项目的大小”:锁定宽度和高度
* Shift +上/下/左/右:移动选中的项目
* Shift + SCROLLER:按鼠标位置缩放
* Ctrl + A:选择所有项目
* Ctrl + G:将选中的项分组或取消分组
* Ctrl + D:复制选中项
* Shift +“绘制线条”:线条梯度水平，垂直45°对角线
* Ctrl + X:剪切选中的项目
* Ctrl + C / V:复制/粘贴选中的项目
