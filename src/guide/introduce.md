# 介绍

## FUXA

FUXA是一款功能强大的基于web的软件，可快速构建和部署可扩展的SCADA, HMI, Dashboard或IIoT系统。使用FUXA，您可以为您的机器和实时数据显示以及自动化工业工厂的控制仪器创建具有个性化设计的现代过程可视化。
![/images/fuxa-editor.png](/images/fuxa-editor.png)

FUXA支持常用的通信标准，如西门子S7协议和OPC UA，允许连接第三方OPC服务器。支持的通信标准列表可以通过开发额外的驱动程序来扩展。

FUXA的软件模型是基于Node.js运行时，你指向一个web浏览器访问编辑器来创建你的应用程序(SCADA/HMI/Dashboard)，作为客户端，你可以运行你的可视化。

## 用户界面

FUXA由两个不同的视图组成:用于编辑项目的FUXA编辑器和用于显示可视化项目产品的FUXA视图。
![/images/fuxa-sample-1.png](/images/fuxa-sample-1.png)
