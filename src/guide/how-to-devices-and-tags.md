# 如何使用设备和标签

## 连接
要添加设备和标签，请转到编辑器中的连接。
![/images/setup-connections.png](/images/setup-connections.png)

## OPCUA
添加并连接OPCUA设备
![/images/fuxa-device.gif](/images/fuxa-device.gif)
要添加OPCUA标签，设备必须连接。
![/images/fuxa-opcuatag.gif](/images/fuxa-opcuatag.gif)
## Modbus
要添加Modbus连接，您必须在插件中安装驱动程序
![/images/fuxa-modbus.gif](/images/fuxa-modbus.gif)
## MQTT
添加一个MQTT连接和一个主题订阅
![/images/fuxa-mqtt.gif](/images/fuxa-mqtt.gif)
## WebAPI
添加WebAPI连接和JSON结果的Tag
![/images/fuxa-webapi.gif](/images/fuxa-webapi.gif)
