# 安装和运行

FUXA是用NodeJS(后端)和Angular(前端)开发的。

您需要安装Node(版本10,12或14)和NPM(版本&gt;=6.11和&lt;7)，最好安装“最新LTS版本:14.17.4(包括npm 6.14.14)”。如果你不需要更改或调试前端，你也可以使用nodejs Version 16。您可能需要Python 2.7或3.9(在Windows中添加到环境变量PATH)，为什么某些包必须从源代码编译。

从NPM安装(第一个选项)

``` sh
npm install -g --unsafe-perm @frangoteam/fuxa
fuxa
```

或者下载最新版本并解压缩(第二个选项)

```sh
cd ./server
npm install
npm start
```

或者在linux中运行docker(第三个选项)

``` sh
docker pull frangoteam/fuxa:latest
docker run -d -p 1881:1881 frangoteam/fuxa:latest
```

应用程序数据(项目)、数据(标签历史)、日志和资源映像的持久存储

``` sh
docker run -d -p 1881:1881 -v fuxa_appdata:/usr/src/app/FUXA/server/_appdata -v fuxa_db:/usr/src/app/FUXA/server/_db -v fuxa_logs:/usr/src/app/FUXA/server/_logs -v fuxa_shapes:/usr/src/app/FUXA/client/assets/lib/svgeditor/shapes -v fuxa_images:/usr/src/app/FUXA/server/_images frangoteam/fuxa:latest
```

打开浏览器(最好是Chrome)，导航到 ```http://localhost:1881```
