# 如何使用相同的视图

## 视图
您可以为重复的组件(如泵和阀门)重用相同的视图。
![/images/fuxa-reuse-view1.gif](/images/fuxa-reuse-view1.gif)

## 内部
你必须将设备定义为内部和一些变量(标签)，并将其绑定到可重用视图的控件。例如标题对话框和输入值的变量。
![/images/fuxa-reuse-view2.gif](/images/fuxa-reuse-view2.gif)

## 事件
然后在每个组件中定义Events以打开对话框，其中定义内部标记与设备的标记之间的连接。
![/images/fuxa-reuse-view3.gif](/images/fuxa-reuse-view3.gif)

如果你愿意，你也可以添加一个确认按钮。
![/images/fuxa-reuse-view4.gif](/images/fuxa-reuse-view4.gif)
