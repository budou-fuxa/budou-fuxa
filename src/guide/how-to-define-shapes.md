# 如何定义形状

形状文件夹位于: ` client/dist/assets/lib/svgeditor/shapes ` (或 ` client/src/lib/svgeditor/shapes `如果你正在调试)，你可以通过创建一个新的 javascript 文件(最好从`my-shapes.js`复制它)或编辑一个现有的文件来添加一个新的形状。

在 javascript 文件中，您可以更改以下内容:

```js
var shapesGroupName = "Shapes"; // used to organize and gropping the shapes, displayed in editor menu as label with expand/collapse

var typeId = "shapes"; // used to identify shapes type, 'shapes' is binded with angular component 'ShapesComponent'
// if you make a new type you have to implement the angular component too
```

在这个数组中你的形状数据，形状对象有以下属性:

```js
var shapes = [{
			name: 'diamond',
			ico: 'assets/lib/svgeditor/shapes/img/shape-diamond.svg',
			content:[
					{ id: '',
						type: 'path',
						attr: { d: 'M 20 0 L 40 20 L 20 40 L 0 20 Z' }
					}
				]
			},
			...
		]

// 'name': is unique for shape type
// 'ico': path icon displayed in editor menu
// 'content': array of svg element
// 'id': element id used if you like make an animation managed in angular component
// 'type': svg element type (path, text, ellipse,...) see svg description
// 'attr': element attribute, depending of type

```

为了设计我的形状，我使用了 Inkscape 应用程序。您可以在 XML 编辑器中看到要在'type'和'attr'中添加的元素和节点属性。

你必须留下剩下的文件内容

如果你创建了一个新的 javascript 形状文件，你也必须添加一个加载器，然后添加到' shapesLoader.js '你的文件名，将被动态加载(不要删除' shapesLoader.js ')

```js
var shapesToLoad = ["my-shapes.js", "your shape file name.js"];
```
如果你创建了一个新的形状类型，比如定义你自己的动画，你也必须实现相应的angular组件。最好在`client/src/app/gauge /shapes/`中查看文件内容。
