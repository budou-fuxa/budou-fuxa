# 如何设计UI布局

## 布局设置

要定义最终用户的布局，请进入编辑器中的布局设置。
![/images/setup-layout.png](/images/setup-layout.png)

在“常规”选项卡中，您可以定义:

* 开始视图，第一个视图(home)显示通过转到 ```http://localhost:1881```
* 缩放，如果启用，您可以使用鼠标滚轮缩放视图，并使用鼠标左键移动视图。
* 对话框模式的输入字段，如果启用将显示一个对话框的输入值控制。
![/images/fuxa-layout-input.gif](/images/fuxa-layout-input.gif)
* 显示导航，设置显示和隐藏标题栏和导航菜单。
* 隐藏开发按钮以在Edit和Home之间切换。
![/images/fuxa-layout.png](/images/fuxa-layout.png)

## 导航侧菜单
在导航侧菜单选项卡中，您可以定义菜单项和不同的样式属性。
![/images/fuxa-layout2.png](/images/fuxa-layout2.png)

## 标题导航栏
在Header导航栏选项卡中，您可以定义是否以及如何显示警报通知项，以及不同的样式属性。
![/images/fuxa-layout3.png](/images/fuxa-layout3.png)
