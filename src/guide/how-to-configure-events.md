# 如何配置事件
在形状和按钮控件中，您可以配置鼠标事件单击、鼠标向下和鼠标向上来执行任务。

## 打开页面
用于在主窗口中显示视图。
![/images/fuxa-events1.gif](/images/fuxa-events1.gif)

## 打开卡片
用于将视图显示为弹出窗口。事件发生时，该牌将显示在鼠标旁边，多张牌可同时显示。例如，同一张卡片可以用来显示更多泵的值。
![/images/fuxa-events2.gif](/images/fuxa-events2.gif)

## 打开对话框
用于将视图显示为对话框，通常用于配置值。对话框将显示在屏幕的顶部。同样的对话框可以用来配置更多泵的值。
![/images/fuxa-events3.gif](/images/fuxa-events3.gif)

## 打开iframe
用于打开窗口iframe(嵌入HTML文档从外部来源)。您可以定义窗口大小和缩放尺度。
![/images/fuxa-events4.gif](/images/fuxa-events4.gif)

## 打开Window
用于在新浏览器中打开一个窗口。您可以定义窗口大小。
![/images/fuxa-events5.gif](/images/fuxa-events5.gif)

## 设置值
用于设置标签值或增加和减少当前值。
![/images/fuxa-events6.gif](/images/fuxa-events6.gif)

## 切换值
用于切换标签值1/0(如果1设置为0，如果0设置为1)。
![/images/fuxa-events7.gif](/images/fuxa-events7.gif)

## 设置从输入和关闭
可用于通过带有OK确认的对话框设置某些值
![/images/fuxa-events8.gif](/images/fuxa-events8.gif)
