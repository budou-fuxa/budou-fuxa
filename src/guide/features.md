# 特性
FUXA不需要任何运行时许可证。您可以构建任意数量和大小的HMI项目，并且无需担心运行时环境的进一步许可。

* 工具箱工业web hmi, web应用程序和仪表板应用程序
* 图形视图生成器完全拖放所见即所得的开发环境
* 100%纯web技术:HTML5, CSS, JavaScript, REST API和websocket通信
* HMI和应用程序可以在触摸屏、PC、平板电脑或智能手机上的所有html5兼容的网络浏览器上运行，不需要插件
* 连接性:OPC-UA, Modbus RTU/TCP, MQTT，西门子S7协议，WebAPI，以太网/IP (Allen Bradley)， BACnet IP(本地完全可定制的连接作为服务可用)
* 服务器运行在Windows, Linux和ARM架构，如树莓派等。
* 集成SQLite数据库引擎的SCADA功能:存储和显示大量的过程数据以及报警事件和上下文数据
* 具有详细权限设置的基于用户的访问

## 通信协议

FUXA平台包括连接器，可实现与西门子plc和OPC UA数据源的直接通信:
* 用于OPC UA连接的客户端
* S7 Protocol to communicate over Ethernet with Siemens CPU 200, 300, 400, 1200, and 1500
* Modbus RTU/TCP, BACnet IP, MQTT，以太网/IP (Allen Bradley)， WebAPI

## 跨平台的完整

本系统设计适用于多种操作系统，安装方便。后端是用NodeJs开发的。用户界面是一个可扩展的HTML5 web前端，使用web技术(HTML5, CSS, Javascript, Angular, SVG)开发，并与所有最新的浏览器兼容。
