# 设置

服务器设置位于`server/_appdata/settings.js`文件中。更改后必须重新启动服务器。

## 身份验证
启用和配置认证。

```js
secureEnabled: true,            // enable or diasable
secretCode: 'frangoteam751',    // secret code to encode the token
tokenExpiresIn: '1h'            // token expiration delay '1h'=1hour, 60=60seconds, '1d'=1day
```

默认用户`admin`的密码是`123456`，当然你可以修改它。
