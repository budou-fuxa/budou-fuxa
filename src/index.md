---
layout: home

title: FUXA
titleTemplate: FUXA

hero:
  name: FUXA
  text: HMI-SCADA
  tagline: 可轻松创建工业WEB HMI, SCADA, WEB应用程序和仪表板应用程序
  image:
    src: /logo.svg
    alt: fuxa
  actions:
   	- theme: brand
      text: 马上开始
      link: /guide/getting-started
    - theme: alt
      text: 介绍
      link: /guide/introduce

features:
  - icon: <span class="i-carbon:settings"></span>
    title: 轻量级和可移植的跨平台应用
    details: FUXA 非常容易从使用树莓派 up 的小型项目扩展到大型设施的可视化。它还提供了强大的完全基于 web 的新工具，允许您创建丰富的可视化，数据日志，警报，通知，趋势和远程设备实时控制。
  - icon: <span class="i-carbon:app-connectivity"></span>
    title: 连接性-包括设备连接器
    details: 它使设备通过行业标准物联网协议 MQTT, OPC-UA, BACnet, Modbus, SiemensS7, WebAPI，以太网/IP (Allen Bradley)连接到一个易于使用的界面。低级协议(如 Serial 或 TCP)可以使您快速地与自定义或专有协议进行交互。
  - icon: <span class="i-carbon:tools-alt"></span>
    title: 可视化工具
    details: 集成图形编辑器使用简单，允许无限的创造力。轻松创建动态 SCADA 仪表板与预定义的小部件，允许任何人创建直观和吸引人的基于 web 的 hmi，工业应用程序和仪表板的数据可视化和实时远程设备控制。
  - icon: <span class="i-carbon:chart-line-data"></span>
    title: 趋势-图表
    details: 创建实时和历史趋势可视化，绘制多个同步数据点。用于分析和导出数据的交互式可视化控件。
  - icon: <span class="i-carbon:service-desk"></span>
    title: 告警管理—事件通知
    details: 报警可以监控模拟和数字信号与固定，可调和跟踪报警限制。将消息(如告警消息或系统信息)转发给指定人员。
  - icon: <span class="i-carbon:user"></span>
    title: 用户管理
    details: 用户权限完全可由系统管理员自定义，用户被分配到权限组。组可以有非常灵活的权限级别，从只读到超级管理员。
  - icon: <span class="i-carbon:report"></span>
    title: 报告
    details: FUXA 中嵌入的报表模块，用于在 PDF 表单中创建指定参数的报表。将报告转发给指定人员。
  - icon: <span class="i-carbon:code"></span>
    title: 脚本
    details: FUXA 中嵌入的报表模块，用于在 PDF 表单中创建指定参数的报表。将报告转发给指定人员。
---

<script setup>
import { onMounted } from 'vue'
import { fetchReleaseTag } from '../.vitepress/utils/fetchReleaseTag.js'

onMounted(() => {
  fetchReleaseTag()
})
</script>
