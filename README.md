# FUXA操作文档

## 本地开发

``` sh
pnpm run dev
```

## 编译
``` sh
pnpm run build
```

## 预览
``` sh
pnpm run serve
```
